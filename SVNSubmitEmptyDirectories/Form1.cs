﻿using System;
using System.Windows.Forms;

namespace SVNSubmitEmptyDirectories
{
    using System.IO;
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            createdFileCount = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.folderBrowserDialog1.ShowDialog();
            this.textBox1.Text = this.folderBrowserDialog1.SelectedPath;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != null)
            {
                CheckData(textBox1.Text);
            }
        }

        public void CheckData(string path)
        {
            DirectoryInfo dir = new DirectoryInfo(path);

            var listDir = dir.GetDirectories();
            //获取不到文件夹 判断有没有文件
            if (listDir.Length == 0)
            {
                //没有文件就写入
                if (dir.GetFiles().Length == 0)
                {
                    createdFileCount++;
                    FileNum.Text = createdFileCount.ToString();
                    //写入文件
                    CreateFile(dir.ToString());
                }
                else
                {
                    return;
                }
            }
            else
            {
                foreach (var item in listDir)
                {

                    CheckData(item.Parent.FullName.ToString() + "\\" + item.Name);

                }
            }
        }

        /// <summary>
        /// 向指定路径写入一个文件
        /// </summary>
        /// <param name="path">要写入文件的路径（只到目录级别，不包含文件名）</param>
        private void CreateFile(string path)
        {
            File.WriteAllText(path + "//.gitkeep", "");
        }

        private int createdFileCount;//写入的文件数量
    }
}
